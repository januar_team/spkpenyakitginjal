<section class="content-header">
    <h1>
        Master Data
        <small>Daftar Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="/disease">Daftar Penyakit</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <form method="post" class="form-horizontal">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Penyakit</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group <?php echo(form_error('code') ? 'has-error' : '') ?>">
                            <label for="code" class="col-sm-2 control-label">Kode</label>
                            <div class="col-sm-4">
                                <input name="code" id="code" placeholder="Kode" type="text"
                                       class="form-control"
                                       value="<?php echo set_value('code', $disease->code) ?>">
                                <?php if (form_error('code')) { ?>
                                    <div class="help-block">
                                        <?php echo form_error('code') ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo(form_error('name') ? 'has-error' : '') ?>">
                            <label for="name" class="col-sm-2 control-label">Nama Penyakit</label>
                            <div class="col-sm-6">
                                <input name="name" id="name" placeholder="Nama Penyakit" type="text"
                                       class="form-control"
                                       value="<?php echo set_value('name', $disease->name) ?>">
                                <?php if (form_error('name')) { ?>
                                    <div class="help-block">
                                        <?php echo form_error('name') ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Deskripsi</label>
                            <div class="col-sm-10">
                            <textarea name="description" id="description" placeholder="Deskripsi"
                                      class="form-control"><?php echo set_value('description', $disease->description) ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <a class="btn btn-default" href="/disease">Batal</a>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>