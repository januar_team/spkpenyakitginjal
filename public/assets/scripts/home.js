jQuery(document).ready(function(){
    var table = $('#dataTables').DataTable({
        responsive: true,
        bFilter: true,
        lengthChange: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 2},
            { responsivePriority: 3, targets: 1},
        ]
    });
});