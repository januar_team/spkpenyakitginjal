<section class="content-header">
    <h1>
        Master Data
        <small>Daftar Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="/disease">Basis Aturan</a></li>
        <li class="active">Tambah</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    <div class="col-lg-12">
        <div class="box box-success">
            <form method="post" class="form-horizontal">
                <div class="box-header">
                    <h3 class="box-title">Daftar Basis Aturan</h3>
                </div>
                <div class="box-body">
                    <div class="form-group <?php echo(form_error('disease_id')? 'has-error' : '')?>">
                        <label for="disease_id" class="col-sm-2 control-label">Nama Penyakit</label>
                        <div class="col-sm-4">
                            <select name="disease_id" id="disease_id" required
                                    class="form-control">
                                <?php foreach ($diseases as $disease) { ?>
                                    <option value="<?php echo $disease->id?>"
                                        <?php echo(set_value('disease_id') == $disease->id ? "selected" : "") ?>><?php echo $disease->name?></option>
                                <?php } ?>
                            </select>
                            <?php if (form_error('disease_id')){ ?>
                                <div class="help-block">
                                    <?php echo form_error('disease_id')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo(form_error('symptom_id')? 'has-error' : '')?>">
                        <label for="symptom_id" class="col-sm-2 control-label">Gejala</label>
                        <div class="col-sm-6">
                            <select name="symptom_id" id="symptom_id" required
                                    class="form-control">
                                <?php foreach ($symptoms as $symptom) { ?>
                                    <option value="<?php echo $symptom->id?>"
                                        <?php echo(set_value('symptom_id') == $symptom->id ? "selected" : "") ?>><?php echo $symptom->name?></option>
                                <?php } ?>
                            </select>
                            <?php if (form_error('symptom_id')){ ?>
                                <div class="help-block">
                                    <?php echo form_error('symptom_id')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo(form_error('probability')? 'has-error' : '')?>">
                        <label for="probability" class="col-sm-2 control-label">Nilai Probabilitas</label>
                        <div class="col-sm-4">
                            <input type="number" min="0" max="1" step="0.01"
                                   name="probability" id="probability" placeholder="Nilai Probabilitas"
                                   class="form-control"
                                   value="<?php echo set_value('probability')?>">
                            <?php if (form_error('probability')){ ?>
                                <div class="help-block">
                                    <?php echo form_error('probability')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a class="btn btn-default" href="/role">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>