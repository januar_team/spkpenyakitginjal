<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    protected $fillable = ['code', 'name', 'description'];

    public $semesta = 0;

    public $p_evidence_if_hypothesis = 0;

    public $bayes = 0;

    protected $appends = ['semesta', 'p_evidence_if_hypothesis', 'bayes'];

    public function symptoms(){
        return $this->belongsToMany(Symptom::class, 'disease_symptoms')->withPivot(['probability'])
            ->using(DiseaseSymptom::class);
    }

    public function getSemestaAttribute(){
        return $this->semesta;
    }

    public function getPEvidenceIfHypothesisAttribute(){
        return $this->p_evidence_if_hypothesis;
    }

    public function getBayesAttribute(){
        return $this->bayes;
    }
}
