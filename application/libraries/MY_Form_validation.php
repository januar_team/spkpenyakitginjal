<?php


class MY_Form_validation extends CI_Form_validation
{
    public function __construct($rules = array())
    {
        parent::__construct($rules);
    }

    public function set_field_data(string $field, array $value){
        $this->_field_data[$field] = $value;
    }

    public function is_unique($str, $field)
    {
        if (substr_count($field, '.') == 2){
            sscanf($field, '%[^.].%[^.].%[^.]', $table, $field, $ignore);
            list($column, $value) = explode('=', $ignore);
            return isset($this->CI->db)
                ? ($this->CI->db->limit(1)->where($field, $str)->where($column . '!=' , $value)
                        ->get($table)->num_rows() === 0)
                : FALSE;
        }else{
            return parent::is_unique($str, $field);
        }
    }
}