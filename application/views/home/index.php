<section class="content-header">
    <h1>
        Home
        <small>Dashboard</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xl-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Penyakit Ginjal</h3>
                </div>
                <div class="box-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Metode Teorema Bayes</h3>
                </div>
                <div class="box-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">
                        <i class="fa fa-list-alt"> </i>
                        History Diagnosa
                    </h3>
                </div>
                <div class="box-body">
                    <table id="dataTables" class="table" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama Pasien</th>
                            <th>Gejala</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($result as $key => $item) {
                            ?>
                            <tr>
                                <td><?php echo($key + 1) ?></td>
                                <td><?php echo $item->patient ?></td>
                                <td>
                                    <ul>
                                        <?php
                                        $temp = [];
                                        foreach ($item->data as $data) {
                                            foreach ($data['symptoms'] as $gejala) {
                                                if (!in_array($gejala['id'], $temp))
                                                    $temp[] = $gejala['id'];
                                                else
                                                    continue;
                                                ?>
                                                <li><?php echo $gejala['name'] ?></li>
                                            <?php }
                                        }?>
                                    </ul>
                                </td>
                                <td>
                                    <a class='btn btn-outline-primary btn-sm'
                                       href='/diagnosa/result/<?php echo $item->id ?>' title='Sunting'>
                                        <i class='fa fa-list'></i> detail</a>
                                </td>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
