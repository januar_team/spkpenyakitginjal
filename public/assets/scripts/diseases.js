jQuery(document).ready(function(){
    var table = $('#dataTables').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        bFilter: true,
        lengthChange: true,
        ajax: {
            url: "/disease",
            type: 'POST',
        },
        columns: [
            {
                data: null,
                className:'control',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                "width": "20px",
                "orderable": false,
            },
            {
                data: 'code',
            },
            {
                data: 'name',
            },
            {
                data: 'description',
                orderable: false,
            },
            {
                width: '15%',
                data: null,
                orderable: false,
                className: 'text-right',
                render: function (data, type, row) {
                    return  "<a class='btn btn-warning btn-sm' href='/disease/edit/"+data.id+"' title='Sunting'><i class='fa fa-pencil'></i></a>" +
                        "<button class='btn-delete btn btn-danger btn-sm' data-item='"+JSON.stringify(data)+"' title='Hapus'><i class='fa fa-trash'></i></button>";
                }
            }
        ],
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 1},
            { responsivePriority: 2, targets: 4},
        ]
    });

    $('#dataTables').on('click', 'button.btn-delete', function(event){
       event.preventDefault();
       var data = $(this).data('item');
       $('#btn-delete').data('id', data.id);
       $('#modal-delete').modal('toggle');
    });

    $('#btn-delete').click(function(){
        var id = $(this).data('id');
        $.ajax({
            url : '/disease/delete/' + id,
            method : 'POST',
            success : function(response){
                if (response.success){
                    toastr.success("Data have been deleted", 'Success');
                    $('#modal-delete').modal('toggle');
                    table.draw();
                } else{
                    toastr.error(response.message, 'Error');
                }
            },
            error : function(xhr){
                toastr.error(xhr.statusText, 'Error');
            }
        });
    })
});