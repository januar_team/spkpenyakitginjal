window._ = require('lodash');

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

require('admin-lte/bower_components/jquery-slimscroll');
require('admin-lte/bower_components/fastclick/lib/fastclick');
require('datatables.net');
require('datatables.net-bs');

require('admin-lte/plugins/iCheck/icheck.min');
require('admin-lte');

require('toastr');