<section class="content-header">
    <h1>
        Master Data
        <small>Daftar Gejala Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="/gejala">Daftar Gejala Penyakit</a> </li>
        <li class="active">Tambah</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-success">
            <form method="post" class="form-horizontal">
                <div class="box-header">
                    <h3 class="box-title">Daftar Gejala Penyakit</h3>
                </div>
                <div class="box-body">
                    <div class="form-group <?php echo(form_error('code')? 'has-error' : '')?>">
                        <label for="code" class="col-sm-2 control-label">Kode</label>
                        <div class="col-sm-4">
                            <input name="code" id="code" placeholder="Kode" type="text"
                                   class="form-control"
                                   value="<?php echo set_value('code')?>">
                            <?php if (form_error('code')){ ?>
                                <div class="help-block">
                                    <?php echo form_error('code')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo(form_error('name')? 'has-error' : '')?>">
                        <label for="name" class="col-sm-2 control-label">Gejala Penyakit</label>
                        <div class="col-sm-6">
                            <input name="name" id="name" placeholder="Gejala Penyakit" type="text"
                                   class="form-control"
                                   value="<?php echo set_value('name')?>">
                            <?php if (form_error('name')){ ?>
                                <div class="help-block">
                                    <?php echo form_error('name')?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a class="btn btn-default" href="/gejala">Batal</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>