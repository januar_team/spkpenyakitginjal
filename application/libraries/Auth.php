<?php


class Auth
{

    const SESSION_NAME = 'auth';

    /**
     * @var MY_Controller
    */
    private $ci;

    /**
     * @var CI_Session
     */
    private $session;

    /**
     * @var UserProvider
    */
    private $userprovider;

    /**
     * @var array
    */
    private $config;

    /**
     * @var \Illuminate\Database\Eloquent\Model
    */
    private $user;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->session =& $this->ci->session;
        $this->ci->load->library('UserProvider');
        $this->userprovider =& $this->ci->userprovider;


        $this->ci->config->load('auth');
        $this->config =& $this->ci->config->config['auth'];
    }

    public function isLogin(){
        if ($this->session->has_userdata(self::SESSION_NAME)){
            return true;
        }else{
            return false;
        }
    }

    public function login(){
        if (!$this->validateLogin()){
            return false;
        }

        return $this->attemptLogin($this->credential());
    }

    public function logout(){
        $this->session->unset_userdata(self::SESSION_NAME);
        redirect($this->config['redirect_unauthenticated'], 'location');
    }

    protected function validateLogin(){
        $this->ci->form_validation->set_rules($this->username(), $this->username('true'), 'required|alpha_numeric');
        $this->ci->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');
        if ($this->ci->form_validation->run()){
            return true;
        }

        return false;
    }

    protected function attemptLogin($credentials){
        $this->user = $this->userprovider->attempt($credentials);

        if ($this->hasValidCredentials($this->user, $credentials)){
            //store session
            $this->session->set_userdata(self::SESSION_NAME, $this->user->attributesToArray());
            $this->authenticate();
            return true;
        }

        // return failed login
        $this->firedFailedResponse();
        return false;
    }

    protected function hasValidCredentials($user, $credentials){
        return ! is_null($user) && $this->userprovider->validateCredentials($user, $credentials);
    }

    protected function credential(){
        return [
            $this->username() => $this->ci->input->post($this->username()),
            'password'      => $this->ci->input->post('password')
        ];
    }

    protected function authenticate(){
        redirect($this->config['redirect_authenticated'], 'location');
    }

    protected function username($label = false){
        $field = 'username';

        return ($label)? ucfirst($field) : $field;
    }

    protected function firedFailedResponse(){
        $this->ci->form_validation->set_field_data($this->username(), [
            'field'		=> $this->username(),
            'label'		=> $this->username(true),
            'rules'		=> 'login',
            'errors'	=> '',
            'is_array'	=> false,
            'keys'		=> $this->username(),
            'postdata'	=> $this->ci->input->post($this->username()),
            'error'		=> 'These credentials do not match our records.'
        ]);
    }

    public function getUser(){
        if ($this->isLogin()){
            if ($this->user)
                return $this->user;

            $data = (object)$this->session->{self::SESSION_NAME};
            return ($this->user = $this->userprovider->retrieveById($data->id));
        }
        return null;
    }

    public function isRole($role){
        return $this->getUser()->role === $role;
    }
}