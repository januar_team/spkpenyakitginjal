<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <style>
        body, html, .wrapper {
            width: 100%;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print()">
<div class="wrapper">
    <section class="invoice">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-success">
                    <div class="form-horizontal">
                        <div class="box-header with-border">
                            <div class="box-title"><h3>Diagnosa Penyakit</h3></div>
                        </div>
                        <div class="box-body">
                            <div class="form-group row">
                                <label for="code" class="col-sm-3 control-label">Nama Pasien</label>
                                <div class="col-sm-4">
                                    <input name="pasien" id="pasien" placeholder="Nama Pasien" type="text" disabled
                                           class="form-control"
                                           value="<?php echo set_value('pasien', $result->patient) ?>">
                                    <?php if (form_error('pasien')) { ?>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('pasien') ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="patient" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-4">
                                        <textarea name="address" id="address" placeholder="Nama Pasien" required
                                                  disabled
                                                  class="form-control"><?php echo set_value('address', $result->address) ?></textarea>
                                    <?php if (form_error('patient')) { ?>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('patient') ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="patient" class="col-sm-3 control-label">Jenis Kelamin</label>
                                <div class="col-sm-4">
                                    <input name="gender" id="gender" required class="form-control" disabled
                                           value="<?php echo set_value('gender', $result->gender) ?>"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="patient" class="col-sm-3 control-label">No. Handphone</label>
                                <div class="col-sm-4">
                                    <input name="phone" id="phone" placeholder="No. Handphone" type="text" required
                                           disabled
                                           class="form-control"
                                           value="<?php echo set_value('phone', $result->phone) ?>">
                                    <?php if (form_error('phone')) { ?>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('phone') ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <h5>Gejala Penyakit</h5>
                            <ul>
                                <?php
                                if ($result instanceof \App\Model\Result) {
                                    $temp = [];
                                    foreach ($result->data as $item) {
                                        foreach ($item['symptoms'] as $symptom) {
                                            if (!in_array($symptom['id'], $temp))
                                                $temp[] = $symptom['id'];
                                            else
                                                continue;
                                            ?>
                                            <li><p><?php echo $symptom['name'] ?></p></li>
                                            <?php
                                        }
                                    }
                                } else {
                                    foreach ($result->symptom as $symptom) {
                                        $item = \App\Model\Symptom::find($symptom['symptom_id']);
                                        if (!$item)
                                            continue;

                                        ?>
                                        <li><p><?php echo $item->name ?></p></li>
                                        <?php
                                    }
                                } ?>
                            </ul>

                            <h4>Hasil</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <th>Nama Penyakit</th>
                                        <th><i>P(Hi)</i></th>
                                        <th>Nilai Semesta</th>
                                        <th>P(Hi)</th>
                                        <th>P(E|Hi)</th>
                                        <th>P(Hi|E)</th>
                                        <th>Nilai Bayes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($result->data as &$disease) {
                                        if (is_array($disease))
                                            $disease = (object)$disease;

                                        if (is_array($disease->symptoms))
                                            $disease->symptoms = collect($disease->symptoms);

                                        ?>
                                        <tr>
                                            <td class="text-nowrap"><?php echo $disease->name ?></td>
                                            <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                    return (is_array($item) ? $item['pivot']['probability'] : $item->pivot->probability);
                                                }) ?></td>
                                            <td><?php echo $disease->semesta ?></td>
                                            <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                    return (is_array($item) ? $item['probability_hypothesis'] : $item->probability_hypothesis);
                                                }) ?></td>
                                            <td><?php echo $disease->p_evidence_if_hypothesis ?></td>
                                            <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                    return (is_array($item) ? $item['probability_hypothesis_if_evidence'] : $item->probability_hypothesis_if_evidence);
                                                }) ?></td>
                                            <td><?php echo $disease->bayes ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <br/>
                            <?php $temp_result = $result->data->first() ?>
                            <div class="col-sm-4">
                                <div class="callout callout-success">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Hasil Diagnosa</div>
                                        <div class="widget-subheading">Nilai Bayes
                                            : <?php echo is_array($temp_result) ? $temp_result['bayes'] : $temp_result->bayes ?></div>
                                    </div>
                                    <h4>
                                        <?php echo is_array($temp_result) ? $temp_result['name'] : $temp_result->name ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>