<?php


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class MY_Controller extends CI_Controller
{
    /**
     * @var View
    */
    public $view;

    /**
     * @var CI_Input
    */
    public $input;

    /**
     * @var CI_Output
    */
    public $output;

    /**
     * @var MY_Form_validation
    */
    public $form_validation;

    /**
     * @var CI_Session
    */
    public $session;

    /**
     * @var Auth
    */
    public $auth;

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library(['form_validation']);
        $this->load->database();
    }

    protected function isPost(){
        return $this->input->method(true) === 'POST';
    }

    /**
     * @param array|object $data
     * @param int $status_code
    */
    protected function json($data, $status_code = 200){
        $this->output
            ->set_content_type('application/json')
            ->set_status_header($status_code)
            ->set_output(($data instanceof Model || $data instanceof Collection) ? $data->toJson() : json_encode($data));
    }
}