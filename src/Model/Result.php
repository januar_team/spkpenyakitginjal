<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    public $timestamps = false;

    protected $fillable = ['patient', 'data', 'address', 'gender', 'phone', 'symptom'];

    protected $casts = [
        'data' => 'collection',
        'symptom' => 'collection'
    ];
}
