<section class="content-header">
    <h1>
        Master Data
        <small>Daftar Gejala Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active">Daftar Gejala Penyakit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Gejala Penyakit</h3>

                <div class="box-tools pull-right" >
                    <a href="/gejala/add" class="btn btn-primary">add</a>
                </div>
            </div>
            <div class="box-body">
                <table id="dataTables" class="table table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode</th>
                        <th>Gejala</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</section>
<!-- Small modal -->

<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah Anda yakin ingin menghapus item</p>
                <p><b id="modal-delete-description"></b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button id="btn-delete" type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>