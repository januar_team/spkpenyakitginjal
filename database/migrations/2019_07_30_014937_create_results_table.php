<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('patient');
            $table->longText('data');
        });

        Schema::create('result_symptoms', function (Blueprint $table){
            $table->unsignedBigInteger('result_id');
            $table->unsignedBigInteger('symptom_id');

            $table->foreign('result_id')->references('id')->on('results');
            $table->foreign('symptom_id')->references('id')->on('symptoms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_symptoms');
        Schema::dropIfExists('results');
    }
}
