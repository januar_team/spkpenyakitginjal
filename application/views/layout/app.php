<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"/>
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="/assets/css/app.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>P</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Sistem</b>Pakar</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/assets/images/avatars/4.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo auth()->getUser()->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/assets/images/avatars/4.jpg" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo auth()->getUser()->name ?>
                                    <small><?php echo auth()->getUser()->username ?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="">
                                    <a href="#" class="btn btn-default btn-flat" style="width: 100%"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
                                </div>
                                <?php echo form_open('/home/logout', ['style' => "display:none", "id" => "logout-form"]) ?>
                                <?php echo form_close() ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/assets/images/avatars/4.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo auth()->getUser()->name ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="<?php echo($class == 'home' ? 'active' : '') ?>">
                    <a href="/">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php if (auth()->isRole('superadmin')){ ?>
                <li class="treeview <?php echo(in_array($class, ['disease', 'gejala', 'role']) ? 'active' : '') ?>">
                    <a href="#">
                        <i class="fa fa-files-o"></i>
                        <span>Master Data</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo($class == 'disease' ? 'active' : '') ?>">
                            <a href="/disease"><i class="fa fa-circle-o"></i> Daftar Penyakit</a>
                        </li>
                        <li class="<?php echo($class == 'gejala' ? 'active' : '') ?>">
                            <a href="/gejala"><i class="fa fa-circle-o"></i> Daftar Gejala Penyakit</a>
                        </li>
                        <li class="<?php echo($class == 'role' ? 'active' : '') ?>">
                            <a href="/role"><i class="fa fa-circle-o"></i> Basis Aturan</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <li class="<?php echo($class == 'diagnosa' ? 'active' : '') ?>">
                    <a href="/diagnosa">
                        <i class="fa fa-table"></i>
                        <span>Proses Diagnosa Penyakit</span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper"><!-- Content Header (Page header) -->
        <?php echo($content ?: '') ?>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2019 <a href="/"><?php echo env('APP_AUTHOR') ?></a>.</strong> All rights
        reserved.
    </footer>
</div>
<script type="text/javascript" src="/assets/scripts/app.js"></script>
<script type="application/javascript">
    jQuery(document).ready(function () {
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            increaseArea: '20%' // optional
        });

        jQuery(document).ready(function () {
            $('.btn-reset').click(function (event) {
                $('input.icheck').iCheck('uncheck');
            })
        })
    });
</script>
<?php if (isset($scripts)) {
    foreach ($scripts as $script) {
        ?>
        <script type="text/javascript" src="<?php echo $script ?>"></script>
        <?php
    }
} ?>
</body>
</html>
