<section class="content-header">
    <h1>
        Diagnosa
        <small>Proses Diagnosa Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Proses Diagnosa Penyakit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <form method="post" class="form-horizontal">
                    <div class="box-header with-border">
                        <div class="box-title">Diagnosa Penyakit - Result</div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="code" class="col-sm-3 control-label">Nama Pasien</label>
                            <div class="col-sm-4">
                                <input name="pasien" id="pasien" placeholder="Nama Pasien" type="text" disabled
                                       class="form-control"
                                       value="<?php echo set_value('pasien', $result->patient) ?>">
                                <?php if (form_error('pasien')) { ?>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('pasien') ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patient" class="col-sm-3 control-label">Alamat</label>
                            <div class="col-sm-4">
                        <textarea name="address" id="address" placeholder="Nama Pasien" required disabled
                                  class="form-control"><?php echo set_value('address', $result->address) ?></textarea>
                                <?php if (form_error('patient')) { ?>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('patient') ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patient" class="col-sm-3 control-label">Jenis Kelamin</label>
                            <div class="col-sm-4">
                                <input name="gender" id="gender" required class="form-control" disabled
                                       value="<?php echo set_value('gender', $result->gender) ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="patient" class="col-sm-3 control-label">No. Handphone</label>
                            <div class="col-sm-4">
                                <input name="phone" id="phone" placeholder="No. Handphone" type="text" required disabled
                                       class="form-control"
                                       value="<?php echo set_value('phone', $result->phone) ?>">
                                <?php if (form_error('phone')) { ?>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('phone') ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <h5>Gejala Penyakit</h5>
                        <ul>
                            <?php
                            if ($result instanceof \App\Model\Result) {
                                $temp = [];
                                foreach ($result->data as $item) {
                                    foreach ($item['symptoms'] as $symptom) {
                                        if (!in_array($symptom['id'], $temp))
                                            $temp[] = $symptom['id'];
                                        else
                                            continue;
                                        ?>
                                        <li><p><?php echo $symptom['name'] ?></p></li>
                                        <?php
                                    }
                                }
                            } else {
                                foreach ($result->symptom as $symptom) {
                                    $item = \App\Model\Symptom::find($symptom['symptom_id']);
                                    if (!$item)
                                        continue;

                                    ?>
                                    <li><p><?php echo $item->name ?></p></li>
                                    <?php
                                }
                            } ?>
                        </ul>

                        <h4>Hasil</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover nowrap" width="100%">
                                <thead>
                                <tr>
                                    <th>Nama Penyakit</th>
                                    <th><i>P(Hi)</i></th>
                                    <th>Nilai Semesta</th>
                                    <th>P(Hi)</th>
                                    <th>P(E|Hi)</th>
                                    <th>P(Hi|E)</th>
                                    <th>Nilai Bayes</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($result->data as &$disease) {
                                    if (is_array($disease))
                                        $disease = (object)$disease;

                                    if (is_array($disease->symptoms))
                                        $disease->symptoms = collect($disease->symptoms);

                                    ?>
                                    <tr>
                                        <td class="text-nowrap"><?php echo $disease->name ?></td>
                                        <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                return (is_array($item) ? $item['pivot']['probability'] : $item->pivot->probability);
                                            }) ?></td>
                                        <td><?php echo $disease->semesta ?></td>
                                        <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                return (is_array($item) ? $item['probability_hypothesis'] : $item->probability_hypothesis);
                                            }) ?></td>
                                        <td><?php echo $disease->p_evidence_if_hypothesis ?></td>
                                        <td><?php echo $disease->symptoms->map(function ($item, $key) {
                                                return (is_array($item) ? $item['probability_hypothesis_if_evidence'] : $item->probability_hypothesis_if_evidence);
                                            }) ?></td>
                                        <td><?php echo $disease->bayes ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <br/>
                        <?php $temp_result = $result->data->first() ?>
                        <div class="col-sm-4">
                            <div class="callout callout-success">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Hasil Diagnosa</div>
                                    <div class="widget-subheading">Nilai Bayes
                                        : <?php echo is_array($temp_result) ? $temp_result['bayes'] : $temp_result->bayes ?></div>
                                </div>
                                <h4>
                                    <?php echo is_array($temp_result) ? $temp_result['name'] : $temp_result->name ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="/diagnosa" class="btn btn-default">Kembali</a>
                            <?php if (!($result instanceof \App\Model\Result)) { ?>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            <?php } ?>
                            <a href="/diagnosa/print<?php echo (($result instanceof \App\Model\Result) ? '/'. $result->id : '') ?>"
                               target="_blank" class="btn btn-primary">
                                <i class="fa fa-print"></i>
                                Print
                            </a>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
</section>
