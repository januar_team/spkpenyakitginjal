<?php
defined('BASEPATH') or exit('No direct script access allowed');

class View 
{
    /**
     * @var CI_Controller
     */
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    /**
     * @param array $data
     * @param string|array $options
    */
    public function load($data = [], $options = ""){
        $view = sprintf("%s/%s", $this->ci->router->class, $this->ci->router->method);
        $use_template = true;
        if(is_array($options)){
            if (array_key_exists('use_template', $options)){
                $use_template = $options['use_template'];
            }

            if (array_key_exists('view', $options)){
                $view = $options['view'];
            }
        }

        if ($use_template){
            $content = $this->ci->load->view($view, $data, true);

            $this->ci->load->view('layout/app', [
                'content' => $content,
                'class' => $this->ci->router->class
            ]);
        }else{
            $this->ci->load->view($view, $data);
        }
    }
}
