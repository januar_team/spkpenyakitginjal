<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo env('APP_NAME') ?> - Login</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="/assets/css/app.css" rel="stylesheet">
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b><?php echo env('APP_NAME') ?></b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php echo form_open('/home/login') ?>
        <div class="form-group has-feedback <?php echo(form_error('username') ? 'has-error' : '') ?>">
            <input name="username" type="text"
                <?php echo set_value('username') ?>
                   class="form-control" placeholder="Username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <?php if (form_error('username')) { ?>
                <div class="help-block">
                    <?php echo form_error('username') ?>
                </div>
            <?php } ?>
        </div>
        <div class="form-group has-feedback <?php echo(form_error('password') ? 'has-error' : '') ?>">
            <input name="password" type="password"
                   value="<?php echo set_value('password') ?>"
                   class="form-control"
                   placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php if (form_error('password')) { ?>
                <div class="help-block">
                    <?php echo form_error('password') ?>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-xs-4 col-lg-offset-8">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
        <?php echo form_close() ?>
    </div>
</div>
<script type="text/javascript" src="/assets/scripts/app.js"></script>
</body>
</html>
