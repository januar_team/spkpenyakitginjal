<?php
class MY_Router extends CI_Router 
{
    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public function getRoute()
    {
        return "/" . $this->class . "/" . $this->method;
    }
}
