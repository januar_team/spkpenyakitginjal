<section class="content-header">
    <h1>
        Diagnosa
        <small>Proses Diagnosa Penyakit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Proses Diagnosa Penyakit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
    <div class="col-lg-12">
        <div class="box box-success">
            <form method="post" class="form-horizontal">
            <div class="box-header with-border">
                <h3 class="box-title">Diagnosa Penyakit</h3>
            </div>
            <div class="box-body">
                <div class="form-group <?php echo(form_error('patient')? 'has-error' : '')?>">
                    <label for="patient" class="col-sm-3 control-label">Nama Pasien</label>
                    <div class="col-sm-3">
                        <input name="patient" id="patient" placeholder="Nama Pasien" type="text"
                               class="form-control"
                               value="<?php echo set_value('patient')?>">
                        <?php if (form_error('patient')){ ?>
                            <div class="help-block">
                                <?php echo form_error('patient')?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="patient" class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-4">
                        <textarea name="address" id="address" placeholder="Nama Pasien" required
                                  class="form-control"><?php echo set_value('address') ?></textarea>
                        <?php if (form_error('patient')) { ?>
                            <div class="invalid-feedback">
                                <?php echo form_error('patient') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="patient" class="col-sm-3 control-label">Jenis Kelamin</label>
                    <div class="col-sm-4">
                        <select name="gender" id="gender" required class="form-control">
                            <option value="Laki-laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="patient" class="col-sm-3 control-label">No. Handphone</label>
                    <div class="col-sm-4">
                        <input name="phone" id="phone" placeholder="No. Handphone" type="text" required
                               class="form-control"
                               value="<?php echo set_value('phone') ?>">
                        <?php if (form_error('phone')) { ?>
                            <div class="invalid-feedback">
                                <?php echo form_error('phone') ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <h4> Gejala Penyakit</h4>
                <!--<table>
                    <tbody>-->
                    <?php
                    foreach ($symptoms as $symptom) {?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?php echo $symptom->name ?></label>
                                <div class="col-sm-6 form-check ">
                                    <input type="radio" class="icheck" value="true" name="role-<?php echo $symptom->id ?>">
                                    <label class="form-check-label">Ya</label>
                                    <input type="radio" class="icheck" value="false" name="role-<?php echo $symptom->id ?>">
                                    <label class="form-check-label">Tidak</label>
                                </div>
                            </div>
                    <?php } ?>
                    <!--</tbody>
                </table>-->
            </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <button class="btn btn-light btn-reset" type="reset">Clear</button>
                        <button class="btn btn-primary" type="submit">Diagnosa</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>